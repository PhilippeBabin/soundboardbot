package events;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;

import audioManagement.AudioManagement;
import audioManagement.GuildMusicManager;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceUpdateEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class DisconnectEvent extends ListenerAdapter {
    private AudioManagement audioManagement;

    public DisconnectEvent() {
        audioManagement = new AudioManagement();
    }

    @Override
    public void onGuildVoiceUpdate(GuildVoiceUpdateEvent event) {
        if (event.getChannelJoined() != null) {
            return;
        }
        User bot = event.getJDA().getUserById(event.getJDA().getSelfUser().getIdLong());
        User disconnectedUser = event.getMember().getUser();
        if (disconnectedUser.equals(bot)) {
            AudioPlayerManager playerManager = audioManagement.getPlayerManager(event.getGuild().getId());
            if (playerManager != null) {
                GuildMusicManager musicManager = audioManagement.getGuildAudioPlayer(event.getGuild(), playerManager);
                musicManager.scheduler.getQueue().clear();
                musicManager.scheduler.nextTrack();
            }
        }
    }
}
