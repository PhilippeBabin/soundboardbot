package soundBoard.resources;

public enum TextMessages
{
    PLAYLIST_CALL("If you see this message, then there is a problem."),
    CONNECTING_VOICE_ERROR("You are not connected to a voice channel! User: "),
    VOICE_CHANNEL_PERM("Can't connect to the voice channel because I don't have the correct permissions!"),
    CHECKING_SOUND("Checking and downloading sound, please wait."),
    REPLACE_OR_REMOVE(" already exists! Please rename it or remove the existing one."),
    SUCCESSFULLY_SAVED(" successfully saved!"),
    VOLUME_WRONG_ARG("Please enter a number between 0 and 150."),
    ALL_WORDS("All available words are: "),
    NO_SAVED_SOUNDS("That name doesn't match. Please add it or enter another name."),
    NAME_TOO_LONG("Name is too long! Limit is set to 70 characters."),
    SOUND_NOT_DOWNLOADED("Error downloading sound. Please check the youtube URL/file or try again (max 30 seconds)."),
    NOT_SUPPORTED("The file download has failed. Please contact my creator."),
    ERROR_FAVORITE("Error removing the favorite sound. Please check the position and try again."),
    REMOVE_SUCCESSFUL_FAV("Removed favorite successfully!"),
    REMOVE_ERROR("Error while deleting file. Please contact my creator."),
    ERROR_VOLUME("Error changing volume. Please contact my creator."),
    TRIGGER_NO_USER("This user doesn't have a trigger."),
    DISABLED_STATE("The bot has been switched to: "),
    ERROR_STATE_CHANGE("Error changing state. Please contact my creator."),
    FOR_ADMIN_ONLY_BUTTON("Sorry, this button is only for admins!"),
    ONLYVOICE_STATE("The bot has been switch to "),
    NOINTERRUPT_STATE("The bot has been switch to "),
    FOR_ADMIN_ONLY_COMMAND("This command is only for admins!"),
    BLACKLIST_CONTAINS_CHANNEL("Channel is already blacklisted!"),
    ERROR_SAVING_BLACKLIST("Error while saving blacklist. Please contact my creator."),
    BLACKLIST_NO_CHANNEL("This channel isn't blacklisted."),
    BLACKLIST_CHANNEL_REMOVED("Channel successfully unblacklisted!"),
    ERROR_REMOVE_BLACKLIST("Error while removing channel from blacklist. Please contact my creator"),
    ONLY_ALPHANUMERIC("Sorry, names can only be alphanumeric (and accents)."),
    ERROR_RENAME("Error while renaming sound file, please contact my creator."),
    DONE("Done!"),
    NO_URL_FILE("You have not provided a URL or file. Please try again."),
    BOTH_URL_FILE("Can't put a URL and file at the same time!"),
    HELP("/addsound <name> <youtube URL or mp3 file> : adds a word detection.\n"
            + "/removesound <name> : removes a word detection.\n"
            + "/addtrigger <name> <user mention> : bot connects at the same time as the mentioned user and plays the <name> sound.\n"
            + "/removetrigger <user mention> : removes one specific trigger for a user\n"
            + "/favorites : see your favorites grid.\n"
            + "/addfavorite <name> <position> : adds a sound to the favorites grid.\n"
            + "/removefavorite <position> : removes the favorite from the grid.\n"
            + "/renamesound <old name> <new name> : renames a specific sound.\n"
            + "/volume <value> : sets the volume for the sounds.\n"
            + "/list : shows all available commands.\n"
            + "/stop : stops the current and all next sounds.\n"
            + "/skip : skips the current playing sound.\n"
            + "/help : shows these commands.\n"
            + "Disable bot button is for admins only!\n"
            + "`Only voice` changes the bot to allow it to reply in public chat or not.\n"
            + "For more info, head to https://gitlab.com/PhilippeBabin/soundboardbot.");

    public final String message;

    private TextMessages(String message)
    {
        this.message = message;
    }
}
