package services;

import java.awt.Color;
import java.util.Collections;

import org.jetbrains.annotations.Nullable;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.utils.FileUpload;
import soundBoard.SoundboardUtils;

public final class MessageService {

    public static void sendMessage(TextChannel channel, String title, String description, Color color) {
        if (SoundboardUtils.checkOrInitDisableBot(channel.getGuild().getId()) ||
                !channel.canTalk()) {
            return;
        }

        EmbedBuilder embed = new EmbedBuilder();
        if (title != null)
            embed.setTitle(title);
        if (description != null)
            embed.setDescription(description);
        embed.setColor(color);
        channel.sendMessageEmbeds(embed.build()).queue();
    }

    public static void sendAttachement(TextChannel channel, FileUpload file) {
        if (SoundboardUtils.checkOrInitDisableBot(channel.getGuild().getId()) ||
                !channel.canTalk()) {
            return;
        }
        channel.sendFiles(Collections.singleton(file)).queue();
    }

    public static void sendReply(SlashCommandInteractionEvent event, String title, @Nullable String description,
            Color color) {
        if (SoundboardUtils.checkOrInitDisableBot(event.getGuild().getId())) {
            return;
        }
        EmbedBuilder embed = new EmbedBuilder();
        embed.setTitle(title);
        embed.setColor(color);
        if (description != null)
            embed.setDescription(description);

        event.replyEmbeds(embed.build()).queue();
    }

    public static void sendReplyEphemeral(SlashCommandInteractionEvent event, String title,
            @Nullable String description, Color color) {
        EmbedBuilder embed = new EmbedBuilder();
        embed.setTitle(title);
        embed.setColor(color);
        if (description != null)
            embed.setDescription(description);

        event.replyEmbeds(embed.build()).setEphemeral(true).queue();
    }
}
