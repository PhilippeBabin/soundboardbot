package events;

import java.util.List;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;

import audioManagement.AudioManagement;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceUpdateEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import services.DatabaseDAO;
import services.SaveService;
import soundBoard.SoundboardUtils;

public class VoiceJoinEvent extends ListenerAdapter {

    private AudioManagement audioManagement;
    private DatabaseDAO database;
    private SaveService saveService;

    public VoiceJoinEvent() {
        audioManagement = new AudioManagement();
        database = new DatabaseDAO();
        saveService = new SaveService();
    }

    @Override
    public void onGuildVoiceUpdate(GuildVoiceUpdateEvent event) {
        if (event.getChannelJoined() == null) {
            return;
        }
        if (event.getOldValue() != null) {
        	if (event.getOldValue().getGuild().equals(event.getGuild())) {
            	return;
            }
        }
        
        String guildId = event.getGuild().getId();
        String userId = event.getMember().getUser().getId();

        if (event.getMember().getUser().isBot())
            return;

        if (SoundboardUtils.checkOrInitDisableBot(guildId))
            return;

        AudioPlayerManager playerManager = audioManagement.getPlayerManager(guildId);

        List<String> allIds = database.getAllTriggers(event.getGuild().getId());

        if (!allIds.contains(userId))
            return;

        String path = database.getPathFromUserId(guildId, userId);
        if (SoundboardUtils.isYoutubeUrl(path)) {
            String newPath = saveService.saveYoutubeSound(guildId, database.getSoundFromUserId(guildId, userId), path);

            if (!database.updateSoundUrl(guildId, path, newPath))
                return;
            path = newPath;
        }
        audioManagement.loadAndPlay(event.getGuild().getDefaultChannel().asTextChannel(), path,
                event.getMember(), playerManager);
    }
}
