package events;

import java.awt.Color;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import audioManagement.AudioManagement;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message.Attachment;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.components.buttons.Button;
import net.dv8tion.jda.api.requests.restaction.CommandListUpdateAction;
import services.DatabaseDAO;
import services.MessageService;
import services.SaveService;
import soundBoard.SoundboardUtils;
import soundBoard.resources.TextMessages;

public class SlashEvent extends ListenerAdapter {
    DatabaseDAO database;
    SaveService saveService;
    AudioManagement audioManagement;

    public SlashEvent() {
        database = new DatabaseDAO();
        saveService = new SaveService();
        audioManagement = new AudioManagement();
    }

    public void createSlashCommands(JDA jda) {
        CommandListUpdateAction commands = jda.updateCommands();

        commands.addCommands(
                Commands.slash("addsound", "add a 30 second or less sound.")
                        .addOptions(new OptionData(OptionType.STRING, "name", "name of the sound.")
                                .setRequired(true))
                        .addOptions(new OptionData(OptionType.STRING, "url", "Youtube URL")
                                .setRequired(false))
                        .addOptions(new OptionData(OptionType.ATTACHMENT, "file", "mp3 file")
                                .setRequired(false)));

        commands.addCommands(
                Commands.slash("addtrigger", "Sets a trigger for a user.")
                        .addOptions(new OptionData(OptionType.STRING, "name", "name of the sound.")
                                .setRequired(true))
                        .addOptions(new OptionData(OptionType.USER, "user", "The user that will trigger the sound.")
                                .setRequired(true)));

        commands.addCommands(
                Commands.slash("addfavorite", "Sets a favorite sound.")
                        .addOptions(new OptionData(OptionType.STRING, "name", "Name of the sound.")
                                .setRequired(true))
                        .addOptions(new OptionData(OptionType.INTEGER, "position", "The position on the grid.")
                                .setRequired(true)));

        commands.addCommands(Commands.slash("favorites", "See favorites grid."));

        commands.addCommands(
                Commands.slash("removesound", "Remove a specific sound")
                        .addOptions(new OptionData(OptionType.STRING, "name", "Name of the sound to remove.")
                                .setRequired(true)));

        commands.addCommands(
                Commands.slash("removetrigger", "Remove a specific trigger")
                        .addOptions(new OptionData(OptionType.USER, "user", "Tag the user to remove the trigger.")
                                .setRequired(true)));

        commands.addCommands(
                Commands.slash("removefavorite", "Remove a sound from the favorites grid.")
                        .addOptions(new OptionData(OptionType.INTEGER, "position", "The position on the grid.")
                                .setRequired(true)));

        commands.addCommands(
                Commands.slash("addblacklist", "FOR ADMINS ONLY! Blacklist a channel.")
                        .addOptions(new OptionData(OptionType.CHANNEL, "channel", "The channel to blacklist.")
                                .setRequired(true)));

        commands.addCommands(
                Commands.slash("removeblacklist", "FOR ADMINS ONLY! Remove a channel from blacklist.")
                        .addOptions(new OptionData(OptionType.CHANNEL, "channel", "The channel to remove.")
                                .setRequired(true)));

        commands.addCommands(
                Commands.slash("renamesound", "Rename a specific sound.")
                        .addOptions(new OptionData(OptionType.STRING, "name", "The name you want to change.")
                                .setRequired(true))
                        .addOptions(new OptionData(OptionType.STRING, "newname", "The new name.")
                                .setRequired(true)));

        commands.addCommands(Commands.slash("list", "See a list of all available sounds."));

        commands.addCommands(
                Commands.slash("volume", "Set the volume between 0% and 150%.")
                        .addOptions(new OptionData(OptionType.INTEGER, "volume", "Volume value")
                                .setRequired(true)));

        commands.addCommands(Commands.slash("skip", "Skips the current playing sound."));

        commands.addCommands(Commands.slash("stop", "Skips the current playing sound."));

        commands.addCommands(Commands.slash("help", "Show information about this bot."));

        commands.queue();

        loadGuildConfigs(jda);
    }

    private void loadGuildConfigs(JDA jda) {
        List<String> allGuildIds = database.getAllGuildsWithConfigs();
        List<String> allFavorites = new ArrayList<String>();
        for (String guildId : allGuildIds) {
            if (jda.getGuildById(guildId) != null) {
                audioManagement.changeVolume(jda.getGuildById(guildId), database.getVolume(guildId));
                SoundboardUtils.disableBotPerGuild.put(guildId, database.getDisabledBot(guildId));
                SoundboardUtils.onlyVoicePerGuild.put(guildId, database.getOnlyVoice(guildId));
                SoundboardUtils.noInterruptSoundPerGuild.put(guildId, database.getNoInterrupt(guildId));

                if (database.getBlackList(guildId) != null) {
                    List<String> blackList = database.getBlackList(guildId);
                    SoundboardUtils.blackListTextChannel.put(guildId, blackList);
                }

                allFavorites = database.getFavorites(guildId);
                Map<String, String> favoritesToName = new HashMap<String, String>();
                int index = 1;
                for (String name : allFavorites) {
                    if (!SoundboardUtils.checkForEmplySlot(name)) {
                        favoritesToName.put("fav" + index, name);
                    }
                    index++;
                }
                SoundboardUtils.favoritesPerGuild.put(guildId, favoritesToName);
            }
        }
    }

    @Override
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        if (event.getGuild() == null)
            return;

        database.initializeConfigs(event.getGuild().getId());
        database.initializeFavorites(event.getGuild().getId());

        switch (event.getName()) {
            case "addsound":
                addCommand(event);
                break;
            case "addtrigger":
                triggerCommand(event);
                break;
            case "addfavorite":
                addFavoriteCommand(event);
                break;
            case "addblacklist":
                addBlackList(event);
                break;
            case "favorites":
                favoritesCommand(event);
                break;
            case "removesound":
                removeSoundCommand(event);
                break;
            case "removetrigger":
                removeTriggerCommand(event);
                break;
            case "removefavorite":
                removeFavoriteCommand(event);
                break;
            case "removeblacklist":
                removeBlackList(event);
                break;
            case "renamesound":
                renameSoundCommand(event);
                break;
            case "list":
                listCommand(event);
                break;
            case "volume":
                volumeCommand(event);
                break;
            case "skip":
                skipCommand(event);
                break;
            case "stop":
                stopCommand(event);
                break;
            case "help":
                helpCommand(event);
                break;
        }
    }

    private void addCommand(SlashCommandInteractionEvent event) {
        MessageService.sendReply(event, TextMessages.CHECKING_SOUND.message, null, Color.blue);
        String guildId = event.getGuild().getId();
        List<String> allSounds = database.getAllSounds(guildId);
        String name = event.getOption("name").getAsString().toLowerCase();

        String url = null;
        Attachment file = null;
        try {
            url = event.getOption("url").getAsString();

        } catch (Exception e) {
        }
        try {
            file = event.getOption("file").getAsAttachment();
        } catch (Exception e) {
        }

        if (url == null && file == null) {
            MessageService.sendMessage(event.getChannel().asTextChannel(), TextMessages.NO_URL_FILE.message, null,
                    Color.red);
        }

        if (name.length() > 70) {
            MessageService.sendMessage(event.getChannel().asTextChannel(), TextMessages.NAME_TOO_LONG.message, null,
                    Color.red);
            return;
        }

        if (name.equalsIgnoreCase("empty slot")) {
            MessageService.sendMessage(event.getChannel().asTextChannel(), "Haha. But no.", null, Color.red);
            return;
        }

        if (allSounds.contains(name.toLowerCase())) {
            MessageService.sendMessage(event.getChannel().asTextChannel(),
                    "Sound" + TextMessages.REPLACE_OR_REMOVE.message, null,
                    Color.red);
            return;
        }

        if (!name.matches("^[ a-zA-Z0-9À-ÿ\u00f1\u00d1]*$")) {
            MessageService.sendMessage(event.getChannel().asTextChannel(), TextMessages.ONLY_ALPHANUMERIC.message, null,
                    Color.red);
            return;
        }

        if (url != null && file == null) {
            if (!SoundboardUtils.isYoutubeUrl(url)) {
                MessageService.sendMessage(event.getChannel().asTextChannel(),
                        TextMessages.SOUND_NOT_DOWNLOADED.message, null,
                        Color.red);
                return;
            }

            String filePath = saveService.saveYoutubeSound(guildId, name, url);
            if (filePath == null) {
                MessageService.sendMessage(event.getChannel().asTextChannel(),
                        TextMessages.SOUND_NOT_DOWNLOADED.message, null,
                        Color.red);
                return;
            }
            if (database.saveSound(event.getGuild().getId(), name, filePath)) {
                MessageService.sendMessage(event.getChannel().asTextChannel(),
                        "Sound" + TextMessages.SUCCESSFULLY_SAVED.message,
                        null, Color.green);
                return;
            }
        }
        if (url == null && file != null) {
            String filePath = saveService.saveMediaFile(file, event.getGuild().getId(), name);
            if (filePath == null) {
                MessageService.sendMessage(event.getChannel().asTextChannel(),
                        TextMessages.SOUND_NOT_DOWNLOADED.message, null,
                        Color.red);
                return;
            }

            if (database.saveSound(event.getGuild().getId(), name, filePath)) {
                MessageService.sendMessage(event.getChannel().asTextChannel(),
                        "Sound" + TextMessages.SUCCESSFULLY_SAVED.message,
                        null, Color.green);
                return;
            }
        }
        if (url != null && file != null) {
            MessageService.sendMessage(event.getChannel().asTextChannel(), TextMessages.BOTH_URL_FILE.message, null,
                    Color.red);
            return;
        }
        MessageService.sendMessage(event.getChannel().asTextChannel(), TextMessages.NOT_SUPPORTED.message, null,
                Color.red);
    }

    private void triggerCommand(SlashCommandInteractionEvent event) {
        String guildId = event.getGuild().getId();
        List<String> allSounds = database.getAllSounds(guildId);
        String name = event.getOption("name").getAsString();

        if (!allSounds.contains(name)) {
            MessageService.sendReply(event, TextMessages.NO_SAVED_SOUNDS.message, null, Color.red);
            return;
        }
        String userId = event.getOption("user").getAsString();
        if (!database.saveTrigger(guildId, userId, name)) {
            MessageService.sendReply(event, "Trigger" + TextMessages.REPLACE_OR_REMOVE.message, null, Color.red);
            return;
        }
        MessageService.sendReply(event, "Trigger" + TextMessages.SUCCESSFULLY_SAVED.message, null, Color.green);
    }

    private void addFavoriteCommand(SlashCommandInteractionEvent event) {
        String guildId = event.getGuild().getId();
        List<String> allSounds = database.getAllSounds(guildId);
        String name = event.getOption("name").getAsString();
        int index = (int) event.getOption("position").getAsLong();
        if (!allSounds.contains(name) || !(index >= 1 && index <= 16)) {
            MessageService.sendReply(event, TextMessages.NO_SAVED_SOUNDS.message, null, Color.red);
            return;
        }

        if (database.saveFavorite(guildId, name, Integer.toString(index))) {
            MessageService.sendReply(event, TextMessages.SUCCESSFULLY_SAVED.message, null, Color.green);
            SoundboardUtils.favoritesPerGuild.get(guildId).put("fav" + index, name);
        }
    }

    private void favoritesCommand(SlashCommandInteractionEvent event) {
        List<String> allFavorites = database.getFavorites(event.getGuild().getId());
        String guildId = event.getGuild().getId();

        if (SoundboardUtils.checkOrInitDisableBot(guildId))
            return;

        event.reply("Favorites:")
                .addActionRow(
                        Button.primary("fav1", "1: " + allFavorites.get(0)),
                        Button.primary("fav2", "2: " + allFavorites.get(1)),
                        Button.primary("fav3", "3: " + allFavorites.get(2)),
                        Button.primary("fav4", "4: " + allFavorites.get(3)))
                .addActionRow(
                        Button.primary("fav5", "5: " + allFavorites.get(4)),
                        Button.primary("fav6", "6: " + allFavorites.get(5)),
                        Button.primary("fav7", "7: " + allFavorites.get(6)),
                        Button.primary("fav8", "8: " + allFavorites.get(7)))
                .addActionRow(
                        Button.primary("fav9", "9: " + allFavorites.get(8)),
                        Button.primary("fav10", "10: " + allFavorites.get(9)),
                        Button.primary("fav11", "11: " + allFavorites.get(10)),
                        Button.primary("fav12", "12: " + allFavorites.get(11)))
                .addActionRow(
                        Button.primary("fav13", "13: " + allFavorites.get(12)),
                        Button.primary("fav14", "14: " + allFavorites.get(13)),
                        Button.primary("fav15", "15: " + allFavorites.get(14)),
                        Button.primary("fav16", "16: " + allFavorites.get(15)))
                .queue();
    }

    private void removeSoundCommand(SlashCommandInteractionEvent event) {
        String guildId = event.getGuild().getId();
        String name = event.getOption("name").getAsString();

        if (!database.getAllSounds(guildId).contains(name)) {
            MessageService.sendReply(event, TextMessages.NO_SAVED_SOUNDS.message, null, Color.red);
            return;
        }

        String path = database.getPathFromName(guildId, name);
        if (!database.removeSound(guildId, name) || !saveService.removeFile(guildId, path)) {
            MessageService.sendReply(event, TextMessages.REMOVE_ERROR.message, null, Color.red);
            return;
        }
        MessageService.sendReply(event, "Sound successfully removed!", null, Color.green);

        List<String> keys = new ArrayList<String>();
        SoundboardUtils.favoritesPerGuild.get(guildId).forEach((k, v) -> {
            if (v.equals(name)) {
                String index = k.split("fav")[1];
                database.removeFavorite(guildId, index);
                keys.add(k);
            }
        });
        for (String key : keys) {
            SoundboardUtils.favoritesPerGuild.get(guildId).remove(key);
        }
    }

    private void renameSoundCommand(SlashCommandInteractionEvent event) {
        String guildId = event.getGuild().getId();
        String oldName = event.getOption("name").getAsString();
        String newName = event.getOption("newname").getAsString();

        if (!database.getAllSounds(guildId).contains(oldName)) {
            MessageService.sendReply(event, TextMessages.NO_SAVED_SOUNDS.message, null, Color.red);
            return;
        }
        String oldPath = database.getPathFromName(guildId, oldName);
        String newUrl = saveService.renameFile(guildId, oldPath, newName);

        if (!database.renameSound(guildId, oldName, newName, newUrl)) {
            MessageService.sendReply(event, TextMessages.ERROR_RENAME.message, null, Color.red);
            return;
        }
        if (database.getFavorites(guildId).contains(oldName)) {
            int index = 1;
            for (String oldFav : database.getFavorites(guildId)) {
                if (oldFav.equals(oldName)) {
                    database.removeFavorite(guildId, Integer.toString(index));
                    database.saveFavorite(guildId, newName, Integer.toString(index));
                }
                index++;
            }
            List<String> allFavorites = new ArrayList<String>();
            allFavorites = database.getFavorites(guildId);
            Map<String, String> favoritesToName = new HashMap<String, String>();
            int index2 = 1;
            for (String name : allFavorites) {
                if (!SoundboardUtils.checkForEmplySlot(name)) {
                    favoritesToName.put("fav" + index2, name);
                }
                index2++;
            }
            SoundboardUtils.favoritesPerGuild.put(guildId, favoritesToName);
        }
        MessageService.sendReply(event, "Rename" + TextMessages.SUCCESSFULLY_SAVED.message, null, Color.green);
    }

    private void removeTriggerCommand(SlashCommandInteractionEvent event) {
        String guildId = event.getGuild().getId();
        String user = event.getOption("user").getAsString();

        if (!database.getAllTriggers(guildId).contains(user)) {
            MessageService.sendReply(event, TextMessages.TRIGGER_NO_USER.message, null, Color.red);
            return;
        }

        if (!database.removeTrigger(guildId, user)) {
            MessageService.sendReply(event, TextMessages.REMOVE_ERROR.message, null, Color.red);
            return;
        }
        MessageService.sendReply(event, "Trigger successfully removed!", null, Color.green);
    }

    private void removeFavoriteCommand(SlashCommandInteractionEvent event) {
        String guildId = event.getGuild().getId();
        String index = event.getOption("position").getAsString();
        if (!database.removeFavorite(guildId, index)) {
            MessageService.sendReply(event, TextMessages.ERROR_FAVORITE.message, null, Color.red);
            return;
        }
        MessageService.sendReply(event, TextMessages.REMOVE_SUCCESSFUL_FAV.message, null, Color.green);
        SoundboardUtils.favoritesPerGuild.get(guildId).remove("fav" + index);
    }

    private void listCommand(SlashCommandInteractionEvent event) {
        List<String> allSounds = database.getAllSounds(event.getGuild().getId());

        List<String> allAvailableNames = new ArrayList<String>();

        int numberOfPages = (int) Math.ceil(allSounds.size() / 10.0);
        String tempString = "";
        if (allSounds.size() > 10) {
            for (int i = 0; i <= 9; i++) {
                tempString += allSounds.get(i) + "\n";
            }
        } else {
            for (int i = 0; i < allSounds.size(); i++) {
                tempString += allSounds.get(i) + "\n";
            }
        }
        allAvailableNames.add(tempString);
        EmbedBuilder embed = new EmbedBuilder();
        embed.setTitle(TextMessages.ALL_WORDS.message + "[" + 1 + "/" + numberOfPages + "]\n");
        embed.setDescription(allAvailableNames.get(0));
        embed.setColor(Color.blue);

        event.replyEmbeds(embed.build())
                .addActionRow(
                        Button.primary("previous", "Previous page"),
                        Button.primary("next", "Next page"))
                .queue();

        if (SoundboardUtils.listIntexPerGuild.get(event.getGuild().getId()) != null)
            SoundboardUtils.listIntexPerGuild.remove(event.getGuild().getId());

        Entry<List<String>, Integer> listWithIndex = new AbstractMap.SimpleEntry<>(allSounds, 0);
        SoundboardUtils.listIntexPerGuild.put(event.getGuild().getId(), listWithIndex);
    }

    private void volumeCommand(SlashCommandInteractionEvent event) {
        int volume = (int) event.getOption("volume").getAsLong();
        if (!(volume >= 0 && volume <= 150)) {
            MessageService.sendReply(event, TextMessages.VOLUME_WRONG_ARG.message, null, Color.red);
            return;
        }
        if (!audioManagement.changeVolume(event.getGuild(), volume)) {
            MessageService.sendReply(event, TextMessages.ERROR_VOLUME.message, null, Color.red);
            return;
        }
        MessageService.sendReply(event, "Volume set to " + volume + ".", null, Color.green);
        database.saveVolume(event.getGuild().getId(), volume);
    }

    private void skipCommand(SlashCommandInteractionEvent event) {
        audioManagement.skipTrack(event.getGuild());
        MessageService.sendReplyEphemeral(event, TextMessages.DONE.message, null, Color.green);
    }

    private void stopCommand(SlashCommandInteractionEvent event) {
        audioManagement.stopAll(event.getGuild());
        MessageService.sendReplyEphemeral(event, TextMessages.DONE.message, null, Color.green);
    }

    private void helpCommand(SlashCommandInteractionEvent event) {
        EmbedBuilder embed = new EmbedBuilder();
        embed.setTitle("Here are the available commands:");
        embed.setDescription(TextMessages.HELP.message);
        embed.setColor(Color.blue);
        event.replyEmbeds(embed.build())
                .addActionRow(
                        Button.primary("onlyVoice", "Only Voice"),
                        Button.primary("disableBot", "Disable Bot"),
                        Button.primary("noInterrupt", "Interrupt playing sound"))
                .queue();
    }

    private void addBlackList(SlashCommandInteractionEvent event) {
        String guildId = event.getGuild().getId();
        String channelId = event.getOption("channel").getAsString();

        if (!event.getMember().getPermissions().contains(Permission.ADMINISTRATOR)) {
            MessageService.sendReply(event, TextMessages.FOR_ADMIN_ONLY_COMMAND.message, null, Color.red);
            return;
        }

        List<String> blackListedChannels = SoundboardUtils.checkOrInitBlacklist(guildId);
        if (blackListedChannels.contains(channelId)) {
            MessageService.sendReply(event, TextMessages.BLACKLIST_CONTAINS_CHANNEL.message, null, Color.red);
            return;
        }

        if (database.saveBlackList(guildId, channelId)) {
            blackListedChannels.add(channelId);
            SoundboardUtils.blackListTextChannel.replace(guildId, blackListedChannels);
            MessageService.sendReply(event, "Channel" + TextMessages.SUCCESSFULLY_SAVED.message, null, Color.green);
            return;
        }
        MessageService.sendReply(event, TextMessages.ERROR_SAVING_BLACKLIST.message, null, Color.red);
    }

    private void removeBlackList(SlashCommandInteractionEvent event) {
        String guildId = event.getGuild().getId();
        String channelId = event.getOption("channel").getAsString();

        List<String> blackListedChannels = SoundboardUtils.blackListTextChannel.get(guildId);
        if (blackListedChannels.isEmpty() || !blackListedChannels.contains(channelId)) {
            MessageService.sendReply(event, TextMessages.BLACKLIST_NO_CHANNEL.message, null, Color.red);
            return;
        }
        if (database.removeBlackList(guildId, channelId)) {
            blackListedChannels.remove(channelId);
            SoundboardUtils.blackListTextChannel.replace(guildId, blackListedChannels);
            MessageService.sendReply(event, TextMessages.BLACKLIST_CHANNEL_REMOVED.message, null, Color.green);
            return;
        }
        MessageService.sendReply(event, TextMessages.ERROR_REMOVE_BLACKLIST.message, null, Color.red);
    }
}
