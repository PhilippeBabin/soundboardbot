package services;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.mysql.cj.jdbc.MysqlDataSource;

public class DatabaseDAO {

    public DatabaseDAO() {
    }

    private MysqlDataSource connectToDatabase() {
        Properties dbProperties = new Properties();
        try {
            dbProperties.load(new FileInputStream("./app.properties"));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        String dbUrl = dbProperties.getProperty("DbUrl");
        String dbUsername = dbProperties.getProperty("DbUsername");
        String dbPassword = dbProperties.getProperty("DbPassword");
        String dbName = dbProperties.getProperty("DbName");
        int dbPort = Integer.parseInt(dbProperties.getProperty("DbPort"));
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setServerName(dbUrl);
        dataSource.setUser(dbUsername);
        dataSource.setPassword(dbPassword);
        dataSource.setDatabaseName(dbName);
        dataSource.setPort(dbPort);

        createTables(dataSource);

        return dataSource;
    }

    private void createTables(MysqlDataSource dataSource) {
        String sqlSounds = "CREATE TABLE IF NOT EXISTS sounds " +
                "(id INT not NULL AUTO_INCREMENT PRIMARY KEY, "
                + "guildId VARCHAR(255) not NULL, " +
                " name VARCHAR(70) not NULL, " +
                " url VARCHAR(255) not NULL" +
                ")";

        String sqlTriggers = "CREATE TABLE IF NOT EXISTS triggers " +
                "(id INT not NULL AUTO_INCREMENT PRIMARY KEY, " +
                "guildId VARCHAR(255) not NULL, " +
                " userId VARCHAR(255) not NULL, " +
                " soundId INT not NULL, " +
                "FOREIGN KEY (soundId) REFERENCES sounds (id) ON DELETE CASCADE" +
                ")";

        String sqlConfigs = "CREATE TABLE IF NOT EXISTS configs " +
                "(" +
                "guildId VARCHAR(255) not NULL PRIMARY KEY, " +
                " onlyvoice TINYINT(1) not NULL DEFAULT 0, " +
                " disabled TINYINT(1) not NULL DEFAULT 0, " +
                " noInterrupt TINYINT(1) not NULL DEFAULT 1, " +
                " volume INT not NULL DEFAULT 100," +
                " stayconnected TINYINT(1)" +
                ")";

        String sqlFavorites = "CREATE TABLE IF NOT EXISTS favorites " +
                "(" +
                "guildId VARCHAR(255) not NULL PRIMARY KEY, " +
                " fav1 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav2 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav3 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav4 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav5 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav6 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav7 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav8 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav9 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav10 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav11 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav12 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav13 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav14 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav15 VARCHAR(70) not NULL DEFAULT 'Empty slot', " +
                " fav16 VARCHAR(70) not NULL DEFAULT 'Empty slot'" +
                ")";

        String sqlBlacklist = "CREATE TABLE IF NOT EXISTS blacklist " +
                "(" +
                "id INT not NULL AUTO_INCREMENT PRIMARY KEY, " +
                " guildId VARCHAR(255) not NULL, " +
                " channelId VARCHAR(255) not NULL" +
                ")";

        try (Connection conn = dataSource.getConnection();
                Statement stmt = conn.createStatement();) {
            stmt.executeUpdate(sqlSounds);
            stmt.executeUpdate(sqlTriggers);
            stmt.executeUpdate(sqlConfigs);
            stmt.executeUpdate(sqlFavorites);
            stmt.executeUpdate(sqlBlacklist);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*
     * All sounds related methods
     */

    public List<String> getAllSounds(String guildId) {
        String sql = "SELECT name FROM sounds WHERE guildId = ? ORDER BY name ASC";
        List<String> allSounds = new ArrayList<String>();
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    allSounds.add(rs.getString("name"));
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allSounds;
    }

    public boolean saveSound(String guildId, String name, String url) {
        String sql = "INSERT INTO sounds (guildId, name, url) VALUES (?, ?, ?)";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setString(2, name);
            ps.setString(3, url);
            ps.executeUpdate();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public boolean removeSound(String guildId, String name) {
        String sql = "DELETE FROM sounds where guildId = ? and name = ?";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setString(2, name);
            ps.execute();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    private int getSoundId(String guildId, String name) {
        String sql = "SELECT id FROM sounds WHERE guildId = ? AND name = ?";
        int id = -1;
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setString(2, name);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    id = rs.getInt("id");
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public String getSoundFromUserId(String guildId, String userId) {
        String sql = "SELECT name FROM sounds as s1 JOIN triggers as s2 ON (s1.id = s2.soundId) WHERE s2.guildId = ? AND s2.userId = ?";
        String sound = "";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setString(2, userId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    sound = rs.getString("name");
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sound;
    }

    public boolean updateSoundUrl(String guildId, String oldUrl, String newUrl) {
        String sql = "UPDATE sounds SET url = ? WHERE guildId = ? AND url = ?";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, newUrl);
            ps.setString(2, guildId);
            ps.setString(3, oldUrl);
            ps.executeUpdate();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean renameSound(String guildId, String oldName, String newName, String newUrl) {
        String sql = "UPDATE sounds SET name = ?, url = ? WHERE guildId = ? AND name = ?";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, newName);
            ps.setString(2, newUrl);
            ps.setString(3, guildId);
            ps.setString(4, oldName);
            ps.executeUpdate();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getPathFromName(String guildId, String name) {
        String sql = "SELECT url FROM sounds WHERE guildId = ? AND name = ?";
        String path = "";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setString(2, name);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    path = rs.getString("url");
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return path;
    }

    public String getPathFromUserId(String guildId, String userId) {
        String sql = "SELECT url FROM sounds as s1 JOIN triggers as s2 ON (s1.id = s2.soundId) WHERE s2.guildId = ? AND s2.userId = ?";
        String path = "";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setString(2, userId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    path = rs.getString("url");
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return path;
    }

    /*
     * All trigger related methods
     */

    public boolean saveTrigger(String guildId, String userId, String name) {
        int soundId = getSoundId(guildId, name);
        if (soundId == -1)
            return false;

        String sql = "INSERT INTO triggers (guildId, userId, soundId) VALUES (?, ?, ?)";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setString(2, userId);
            ps.setInt(3, soundId);
            ps.executeUpdate();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean removeTrigger(String guildId, String userId) {
        String sql = "DELETE FROM triggers WHERE guildId = ? and userId = ?";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setString(2, userId);
            ps.execute();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public List<String> getAllTriggers(String guildId) {
        String sql = "SELECT userId FROM triggers WHERE guildId = ?";
        List<String> allTriggers = new ArrayList<String>();
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    allTriggers.add(rs.getString("userId"));
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allTriggers;
    }

    /*
     * All favorites related methods
     */

    public boolean initializeFavorites(String guildId) {
        String sql = "INSERT IGNORE into favorites SET guildId = ?";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.executeUpdate();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<String> getFavorites(String guildId) {
        String sql = "SELECT fav1,fav2,fav3,fav4,fav5,fav6,fav7,fav8,fav9,fav10,fav11,fav12,fav13,fav14,fav15,fav16 FROM favorites WHERE guildId = ?";
        List<String> allFavorites = new ArrayList<String>();
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    allFavorites.add(rs.getString("fav1"));
                    allFavorites.add(rs.getString("fav2"));
                    allFavorites.add(rs.getString("fav3"));
                    allFavorites.add(rs.getString("fav4"));
                    allFavorites.add(rs.getString("fav5"));
                    allFavorites.add(rs.getString("fav6"));
                    allFavorites.add(rs.getString("fav7"));
                    allFavorites.add(rs.getString("fav8"));
                    allFavorites.add(rs.getString("fav9"));
                    allFavorites.add(rs.getString("fav10"));
                    allFavorites.add(rs.getString("fav11"));
                    allFavorites.add(rs.getString("fav12"));
                    allFavorites.add(rs.getString("fav13"));
                    allFavorites.add(rs.getString("fav14"));
                    allFavorites.add(rs.getString("fav15"));
                    allFavorites.add(rs.getString("fav16"));
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allFavorites;
    }

    public boolean saveFavorite(String guildId, String name, String index) {
        String columnName = "fav" + index;
        String sql = "INSERT INTO favorites (guildId, " + columnName + ") VALUES (?, ?) ON DUPLICATE KEY UPDATE "
                + columnName + " = ?";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setString(2, name);
            ps.setString(3, name);
            ps.executeUpdate();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean removeFavorite(String guildId, String index) {
        String columnName = "fav" + index;
        String sql = "INSERT INTO favorites (guildId, " + columnName
                + ") VALUES (?, 'Empty slot') ON DUPLICATE KEY UPDATE " + columnName + " = 'Empty slot'";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.execute();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /*
     * All configs related methods
     */

    public boolean saveVolume(String guildId, int volume) {
        String sql = "INSERT INTO configs (guildId, volume) VALUES (?, ?) ON DUPLICATE KEY UPDATE  volume = ?";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setInt(2, volume);
            ps.setInt(3, volume);
            ps.executeUpdate();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean saveOnlyVoice(String guildId, boolean onlyVoice) {
        String sql = "INSERT INTO configs (guildId, onlyvoice) VALUES (?, ?) ON DUPLICATE KEY UPDATE onlyvoice = ?";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setBoolean(2, onlyVoice);
            ps.setBoolean(3, onlyVoice);
            ps.executeUpdate();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean saveDisabledBot(String guildId, boolean disableBot) {
        String sql = "INSERT INTO configs (guildId, disabled) VALUES (?, ?) ON DUPLICATE KEY UPDATE disabled = ?";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setBoolean(2, disableBot);
            ps.setBoolean(3, disableBot);
            ps.executeUpdate();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean saveNoInterrupt(String guildId, boolean noInterrupt) {
        String sql = "INSERT INTO configs (guildId, noInterrupt) VALUES (?, ?) ON DUPLICATE KEY UPDATE noInterrupt = ?";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setBoolean(2, noInterrupt);
            ps.setBoolean(3, noInterrupt);
            ps.executeUpdate();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getVolume(String guildId) {
        String sql = "SELECT volume FROM configs WHERE guildId = ?";
        int volume = 100;
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    volume = rs.getInt("volume");
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return volume;
    }

    public boolean getOnlyVoice(String guildId) {
        String sql = "SELECT onlyvoice FROM configs WHERE guildId = ?";
        boolean onlyVoice = false;
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    onlyVoice = rs.getBoolean("onlyvoice");
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return onlyVoice;
    }

    public boolean getDisabledBot(String guildId) {
        String sql = "SELECT disabled FROM configs WHERE guildId = ?";
        boolean disabledBot = false;
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    disabledBot = rs.getBoolean("disabled");
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return disabledBot;
    }

    public boolean getNoInterrupt(String guildId) {
        String sql = "SELECT noInterrupt FROM configs WHERE guildId = ?";
        boolean noInterrupt = false;
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    noInterrupt = rs.getBoolean("noInterrupt");
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return noInterrupt;
    }

    public List<String> getAllGuildsWithConfigs() {
        String sql = "SELECT guildId FROM configs";
        List<String> allGuilds = new ArrayList<String>();

        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    allGuilds.add(rs.getString("guildId"));
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allGuilds;
    }

    public boolean initializeConfigs(String guildId) {
        String sql = "INSERT IGNORE into configs SET guildId = ?";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.executeUpdate();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /*
     * All blackList related methods
     */

    public List<String> getBlackList(String guildId) {
        String sql = "SELECT channelId FROM blacklist WHERE guildId = ?";
        List<String> allBlacklists = new ArrayList<String>();

        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    allBlacklists.add(rs.getString("channelId"));
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allBlacklists;
    }

    public boolean saveBlackList(String guildId, String channelId) {
        String sql = "INSERT INTO blacklist (guildId, channelId) VALUES (?, ?)";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setString(2, channelId);
            ps.executeUpdate();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean removeBlackList(String guildId, String channelId) {
        String sql = "DELETE FROM blacklist WHERE guildId = ? AND channelId = ?";
        try (Connection con = connectToDatabase().getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, guildId);
            ps.setString(2, channelId);
            ps.execute();
            ps.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
