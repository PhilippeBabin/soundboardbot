package audioManagement;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;

import java.util.LinkedList;
import java.util.Queue;

/**
 * This class schedules tracks for the audio player. It contains the queue of
 * tracks.
 * 
 * This code came from the lavaplayer github:
 * https://github.com/sedmelluq/lavaplayer/tree/master/demo-jda/src/main/java/com/sedmelluq/discord/lavaplayer/demo/jda
 */
public class TrackScheduler extends AudioEventAdapter {
    private final AudioPlayer player;
    private final Queue<AudioTrack> queue;

    public Queue<AudioTrack> getQueue() {
        return queue;
    }

    /**
     * @param player The audio player this scheduler uses
     */
    public TrackScheduler(AudioPlayer player) {
        this.player = player;
        this.queue = new LinkedList<>();
    }

    /**
     * Add the next track to queue and play right away if nothing is in the queue.
     *
     * @param track The track to play and add to queue.
     */
    public void queue(AudioTrack track, boolean noInterrupt) {
        queue.add(track);
        if (noInterrupt) {
            if (queue.size() > 0 && player.getPlayingTrack() == null) {
                player.startTrack(queue.poll(), true);
            }
            return;
        }
        
        if (!player.startTrack(queue.poll(), false))
            queue.offer(track);
    }

    /**
     * Start the next track, stopping the current one if it is playing.
     */
    public boolean nextTrack() {
        // Start the next track, regardless of if something is already playing or not.
        // In case queue was empty, we are
        // giving null to startTrack, which is a valid argument and will simply stop the
        // player.
        return player.startTrack(queue.poll(), false);
    }

    @Override
    public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
        // Only start the next track if the end reason is suitable for it (FINISHED or
        // LOAD_FAILED)
        if (endReason.mayStartNext) {
            if (!nextTrack()) {
                AudioManagement.disconnectFromVoiceChannel();
            }
            return;
        }
        if (queue.size() == 0 && player.getPlayingTrack() == null) {
            AudioManagement.disconnectFromVoiceChannel();
        }
    }

    public boolean setVolume(int volume) {
        if (volume <= 150 && volume >= 0) {
            player.setVolume(volume);
            return true;
        }
        return false;
    }

    public int getVolume() {
        return player.getVolume();
    }
}
