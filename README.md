# SoundBoardBot

this is a basic discord bot that acts like a soundboard. <br>

Here are the available commands:<br>
/addsound \<name> \<youtube URL or mp3 file> : adds a word detection.<br>
/removesound \<name> : removes a word detection.<br>
/addtrigger \<name> \<user mention> : bot connects at the same time as the mentioned user and plays the <name> sound.<br>
/removetrigger \<user mention> : removes one specific trigger for a user<br>
/favorites : see your favorites grid.<br>
/addfavorite \<name> \<position> : adds a sound to the favorites grid.<br>
/removefavorite \<position> : removes the favorite from the grid.<br>
/volume \<value> : sets the volume for the sounds.<br>
/list : shows all available commands.<br>
/stop : stops the current and all next sounds.<br>
/skip : skips the current playing sound.<br>
/help : shows these commands.<br>
Disable bot button is for admins only!<br>
Only voice changes the bot to allow it to reply in public or not.<br>

<br>
This bot recognizes the words in a text message, and if the user who wrote the word is connected to a voice channel, the bot will come and play the youtube video sound. If the user isn't connected, the bot will send the link in the same text channel.<br>

This is only the beginning of the bot, I do host it myself but only for a few servers. If you want to host it yourself, I'll soon put a .jar file up.<br>

<b>The bot only works with x64 systems, so ARM isn't supported (yet).</b><br>


## Installation

I'm working on an automatic installer to auto-deploy everything needed to self-host this bot.<br>
<br>
<br>
If you really want to launch it before hand, here's what you need to do:<br>
    1. First, generate the .jar file (I'll upload one soon).<br>
    2. Then, you'll need a mySQL or mariaDB database ready. <br>
    3. Then, create a new file called "app.properties" and enter the following info:<br>
```
token=1234
DbUrl=192.168.1.1
DbName=
DbUsername=
DbPassword=
```
Of course, enter the info for your database and your bot token (get your bot token here: https://discord.com/developers/applications). <br>
    4. You should be good to go! Open a terminal, write `java -jar <jarfile>.jar` and make sure the app.properties file is in the same directory as the jar file.<br>
<br>
<b>NOTE!</b> Your bot will need some OAuth2 authorizations: in 'scopes' select 'bot' AND 'applications.commands'. Then in bot permissions, choose: Send Messages, Embed Links, Attach Files, Read Message History, Use Slash Commands, Connect, Speak, Use Voice Activity.
