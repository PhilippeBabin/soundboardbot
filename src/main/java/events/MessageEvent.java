package events;

import java.awt.Color;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;

import audioManagement.AudioManagement;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.entities.channel.middleman.AudioChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.utils.FileUpload;
import services.DatabaseDAO;
import services.MessageService;
import services.SaveService;
import soundBoard.SoundboardUtils;

public class MessageEvent extends ListenerAdapter {

    private AudioManagement audioManagement;
    private DatabaseDAO database;
    private SaveService saveService;

    public MessageEvent() {
        audioManagement = new AudioManagement();
        database = new DatabaseDAO();
        saveService = new SaveService();
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getAuthor().isBot())
            return;

        String guildId = event.getGuild().getId();
        AudioPlayerManager playerManager = audioManagement.getPlayerManager(event.getGuild().getId());

        List<String> blackList = SoundboardUtils.blackListTextChannel.get(guildId);
        if (blackList != null) {
            if (SoundboardUtils.blackListTextChannel.get(guildId).contains(event.getChannel().getId()))
                return;
        }
        if (!SoundboardUtils.checkOrInitDisableBot(guildId)) {
            checkMessageForWords(event.getMessage(), playerManager);
        }
    }

    private void checkMessageForWords(Message message, AudioPlayerManager playerManager) {
        ChannelType channelType = message.getChannel().getType();
        if (channelType.equals(ChannelType.VOICE)) {
            return;
        }
        TextChannel channel = message.getChannel().asTextChannel();
        String guildId = message.getGuild().getId();

        if (message.getContentRaw().equals("All available words are:")) {
            channel.sendMessage("Haha. Smartass.").queue();
            return;
        }

        // gets all the different trigger words
        List<String> names = database.getAllSounds(guildId);

        // check all the trigger words in the message and store them in a map with the
        // first index of the trigger word
        Map<Integer, String> wordsInSentence = new HashMap<>();
        for (String name : names) {
            int indexValue = SoundboardUtils.isContain(message.getContentRaw().toLowerCase(), name.toLowerCase());
            if (indexValue != -1) {
                wordsInSentence.put(indexValue, name);
            }
        }

        // for each character in the string message, find the word in the map that's the
        // lowest index (so the first word), and play it
        for (int i = 0; i < message.getContentRaw().toLowerCase().length(); i++) {
            // if the word is not null (so if it's the first word)
            if (wordsInSentence.get(i) != null) {
                String nameInMessage = wordsInSentence.get(i);

                String path = database.getPathFromName(guildId, nameInMessage);

                if (SoundboardUtils.isYoutubeUrl(path)) {
                    MessageService.sendMessage(channel, "Converting sound, please wait.", null, Color.blue);
                    String newPath = saveService.saveYoutubeSound(guildId, nameInMessage, path);
                    if (newPath == null) {
                        MessageService.sendMessage(channel,
                                "Sound cannot be downloaded. Please try again or replace the sound.", null, Color.red);
                        return;
                    }

                    if (!database.updateSoundUrl(guildId, path, newPath))
                        return;
                    path = newPath;
                }

                // we check if a user is mentioned
                if (message.getMentions().getMembers().size() > 0) {
                    Member member = message.getMentions().getMembers().get(0);
                    AudioChannel connectedChannelMentioned = member.getVoiceState().getChannel();
                    if (connectedChannelMentioned != null) {
                        audioManagement.loadAndPlay(channel, path, member, playerManager);
                    }
                    return;
                }

                // we check if the user who wrote the message is connected on voice channel
                AudioChannel connectedChannel = message.getMember().getVoiceState().getChannel();
                if (connectedChannel != null) {
                    audioManagement.loadAndPlay(channel, path, message.getMember(),
                            playerManager);
                    return;
                }

                // if user is not connected and that onlyvoice is false
                if (!SoundboardUtils.checkOrInitOnlyVoice(channel.getGuild().getId())) {
                    if (!SoundboardUtils.isYoutubeUrl(path)) {
                        File file = new File(path);
                        FileUpload fileUpload = FileUpload.fromData(file);
                        MessageService.sendAttachement(channel, fileUpload);
                        return;
                    }
                    MessageService.sendMessage(channel, path, null, Color.BLACK);
                }
                break;
            }
        }
        return;
    }
}
