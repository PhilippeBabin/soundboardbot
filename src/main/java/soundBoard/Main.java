package soundBoard;

import java.io.FileInputStream;
import java.util.Properties;

import events.ButtonEvent;
import events.DisconnectEvent;
import events.MessageEvent;
import events.SlashEvent;
import events.VoiceJoinEvent;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

public class Main extends ListenerAdapter {
    public static void main(String[] args) {
        
        Properties botTokenProperty = new Properties();
        try {
            botTokenProperty.load(new FileInputStream("./app.properties"));
            String token = botTokenProperty.getProperty("token");
            
            SlashEvent slashEvent = new SlashEvent();
            
            JDA jda = JDABuilder.createDefault(token)
                    .addEventListeners(slashEvent, new ButtonEvent(), new MessageEvent(), new VoiceJoinEvent(), new DisconnectEvent())
                    .disableCache(CacheFlag.ACTIVITY).setActivity(Activity.listening("/help"))
                    .enableIntents(GatewayIntent.MESSAGE_CONTENT)
                    .build().awaitReady();
            
            slashEvent.createSlashCommands(jda);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Token or properties file isn't correct! Please re-launch the bot with the correct information.");
            System.exit(1);
        }
    }
}
