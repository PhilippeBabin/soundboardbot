package events;

import java.awt.Color;
import java.util.ArrayList;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;

import audioManagement.AudioManagement;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import services.DatabaseDAO;
import services.MessageService;
import soundBoard.SoundboardUtils;
import soundBoard.resources.TextMessages;

public class ButtonEvent extends ListenerAdapter {

    private AudioManagement audioManagement;
    private DatabaseDAO database;

    public ButtonEvent() {
        audioManagement = new AudioManagement();
        database = new DatabaseDAO();
    }

    @Override
    public void onButtonInteraction(ButtonInteractionEvent event) {
        String type = event.getComponentId();

        AudioPlayerManager playerManager = audioManagement.getPlayerManager(event.getGuild().getId());

        try {
            event.deferEdit().queue();
        } catch (Exception e) {
            System.out.println("Button response too slow. Ignoring error.");
        }

        switch (type) {
            case "fav1":
            case "fav2":
            case "fav3":
            case "fav4":
            case "fav5":
            case "fav6":
            case "fav7":
            case "fav8":
            case "fav9":
            case "fav10":
            case "fav11":
            case "fav12":
            case "fav13":
            case "fav14":
            case "fav15":
            case "fav16":
                playFavoriteSound(event, type, playerManager);
                break;
            case "onlyVoice":
                changeOnlyVoiceWithReply(event);
                break;
            case "disableBot":
                changeDisableBotWithReply(event);
                break;
            case "noInterrupt":
                changeNoInitWithReply(event);
                break;
            case "previous":
                previousPage(event);
                break;
            case "next":
                nextPage(event);
                break;
        }
    }

    private void previousPage(ButtonInteractionEvent event) {
        Entry<List<String>, Integer> soundsWithIndex = SoundboardUtils.listIntexPerGuild.get(event.getGuild().getId());
        if (soundsWithIndex == null)
            return;

        if (soundsWithIndex.getValue() == 0)
            return;

        int currentIndex = soundsWithIndex.getValue();
        int startIndex = (currentIndex - 1) * 10;
        int endIndex = (currentIndex * 10) - 1;

        List<String> allSounds = soundsWithIndex.getKey();
        List<String> soundsToShow = new ArrayList<>();

        String tempString = "";
        if (endIndex >= allSounds.size()) {
            for (int i = startIndex; i < allSounds.size(); i++) {
                tempString += allSounds.get(i) + "\n";
            }
        } else {
            for (int i = startIndex; i <= endIndex; i++) {
                tempString += allSounds.get(i) + "\n";
            }
        }
        soundsToShow.add(tempString);

        int numberOfPages = (int) Math.ceil(allSounds.size() / 10.0);

        EmbedBuilder embed = new EmbedBuilder();
        embed.setTitle(TextMessages.ALL_WORDS.message + "[" + (currentIndex) + "/" + numberOfPages + "]\n");
        embed.setDescription(soundsToShow.get(0));
        embed.setColor(Color.blue);
        event.getMessage().editMessageEmbeds(embed.build()).queue();

        if (SoundboardUtils.listIntexPerGuild.get(event.getGuild().getId()) != null)
            SoundboardUtils.listIntexPerGuild.remove(event.getGuild().getId());

        Entry<List<String>, Integer> listWithIndex = new AbstractMap.SimpleEntry<>(allSounds, currentIndex - 1);
        SoundboardUtils.listIntexPerGuild.put(event.getGuild().getId(), listWithIndex);
    }

    private void nextPage(ButtonInteractionEvent event) {
        Entry<List<String>, Integer> soundsWithIndex = SoundboardUtils.listIntexPerGuild.get(event.getGuild().getId());
        if (soundsWithIndex == null)
            return;

        List<String> allSounds = soundsWithIndex.getKey();

        if (soundsWithIndex.getValue() == (int) Math.ceil(allSounds.size() / 10.0))
            return;

        int currentIndex = soundsWithIndex.getValue();
        int startIndex = (currentIndex + 1) * 10;
        int endIndex = ((currentIndex + 1) * 10) + 9;

        List<String> soundsToShow = new ArrayList<>();

        String tempString = "";
        if (endIndex >= allSounds.size()) {
            for (int i = startIndex; i < allSounds.size(); i++) {
                tempString += allSounds.get(i) + "\n";
            }
        } else {
            for (int i = startIndex; i <= endIndex; i++) {
                tempString += allSounds.get(i) + "\n";
            }
        }
        soundsToShow.add(tempString);

        int numberOfPages = (int) Math.ceil(allSounds.size() / 10.0);

        EmbedBuilder embed = new EmbedBuilder();
        embed.setTitle(TextMessages.ALL_WORDS.message + "[" + (currentIndex + 2) + "/" + numberOfPages + "]\n");
        embed.setDescription(soundsToShow.get(0));
        embed.setColor(Color.blue);
        event.getMessage().editMessageEmbeds(embed.build()).queue();

        if (SoundboardUtils.listIntexPerGuild.get(event.getGuild().getId()) != null)
            SoundboardUtils.listIntexPerGuild.remove(event.getGuild().getId());

        Entry<List<String>, Integer> listWithIndex = new AbstractMap.SimpleEntry<>(allSounds, currentIndex + 1);
        SoundboardUtils.listIntexPerGuild.put(event.getGuild().getId(), listWithIndex);
    }

    private void playFavoriteSound(ButtonInteractionEvent event, String fav, AudioPlayerManager playerManager) {
        String guildId = event.getGuild().getId();
        Member member = event.getMember();

        if (member.getVoiceState().getChannel() == null) {
            MessageService.sendMessage(event.getChannel().asTextChannel(),
                    TextMessages.CONNECTING_VOICE_ERROR.message + member.getEffectiveName(),
                    null, Color.RED);
            return;
        }

        Map<String, String> favoritesToName = SoundboardUtils.favoritesPerGuild.get(guildId);
        String name = favoritesToName.get(fav);
        if (name == null)
            return;

        String path = database.getPathFromName(guildId, name);
        audioManagement.loadAndPlay(event.getChannel().asTextChannel(), path, member, playerManager);
    }

    private void changeOnlyVoiceWithReply(ButtonInteractionEvent event) {
        String guildId = event.getGuild().getId();
        Boolean onlyVoice = !database.getOnlyVoice(guildId);
        if (database.saveOnlyVoice(guildId, onlyVoice)) {
            SoundboardUtils.onlyVoicePerGuild.replace(guildId, onlyVoice);

            EmbedBuilder embed = new EmbedBuilder();
            embed.setTitle(
                    TextMessages.ONLYVOICE_STATE.message + (onlyVoice ? "NOT react in chat." : "react in chat."));
            embed.setColor(Color.blue);
            event.getChannel().sendMessageEmbeds(embed.build()).queue();
            return;
        }
        MessageService.sendMessage(event.getChannel().asTextChannel(), TextMessages.ERROR_STATE_CHANGE.message, null,
                Color.red);
    }

    private void changeNoInitWithReply(ButtonInteractionEvent event) {
        String guildId = event.getGuild().getId();
        Boolean noInterrupt = !database.getNoInterrupt(guildId);
        if (database.saveNoInterrupt(guildId, noInterrupt)) {
            SoundboardUtils.noInterruptSoundPerGuild.replace(guildId, noInterrupt);

            EmbedBuilder embed = new EmbedBuilder();
            embed.setTitle(TextMessages.NOINTERRUPT_STATE.message
                    + (noInterrupt ? "NOT interrupt sounds." : "interrupt sounds."));
            embed.setColor(Color.blue);
            event.getChannel().sendMessageEmbeds(embed.build()).queue();
            return;
        }
        MessageService.sendMessage(event.getChannel().asTextChannel(), TextMessages.ERROR_STATE_CHANGE.message, null,
                Color.red);
    }

    private void changeDisableBotWithReply(ButtonInteractionEvent event) {
        String guildId = event.getGuild().getId();
        if (event.getMember().getPermissions().contains(Permission.ADMINISTRATOR)) {
            Boolean disabedBot = !database.getDisabledBot(guildId);
            if (database.saveDisabledBot(guildId, disabedBot)) {
                SoundboardUtils.disableBotPerGuild.replace(guildId, disabedBot);

                EmbedBuilder embed = new EmbedBuilder();
                embed.setTitle(TextMessages.DISABLED_STATE.message + (disabedBot ? "disabled" : "enabled"));
                embed.setColor(Color.blue);
                event.getChannel().sendMessageEmbeds(embed.build()).queue();
                ;
                return;
            }
            MessageService.sendMessage(event.getChannel().asTextChannel(), TextMessages.ERROR_STATE_CHANGE.message,
                    null,
                    Color.red);
            return;
        }
        MessageService.sendMessage(event.getChannel().asTextChannel(), TextMessages.FOR_ADMIN_ONLY_BUTTON.message, null,
                Color.red);
    }
}
