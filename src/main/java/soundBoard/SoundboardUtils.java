package soundBoard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import services.DatabaseDAO;

public final class SoundboardUtils {
    public static final Map<String, Boolean> onlyVoicePerGuild = new HashMap<>();
    public static final Map<String, Boolean> disableBotPerGuild = new HashMap<>();
    public static final Map<String, Map<String, String>> favoritesPerGuild = new HashMap<>();
    public static final Map<String, Boolean> noInterruptSoundPerGuild = new HashMap<>();
    public static final Map<String, List<String>> blackListTextChannel = new HashMap<>();
    public static final Map<String, Map.Entry<List<String>, Integer>> listIntexPerGuild = new HashMap<>();

    public static boolean isYoutubeUrl(String youTubeURL) {
        String pattern = "^(http(s)?:\\/\\/)?((w){3}.)?youtu(be|.be)?(\\.com)?\\/.+";
        return (!youTubeURL.isEmpty() && youTubeURL.matches(pattern));
    }

    public static int isContain(String source, String subItem) {
        String pattern = "(?<![\\w\\d])" + subItem + "(?![\\w\\d])";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(source);
        if (m.find()) {
            return m.start();
        }
        return -1;
    }
    
    public static boolean isMP3File(String path) {
        Pattern p = Pattern.compile(".(mp3)$");
        Matcher m = p.matcher(path);
        return m.find();
    }
    
    public static boolean checkOrInitOnlyVoice(String guildId) {
        if (onlyVoicePerGuild.get(guildId) == null) {
            onlyVoicePerGuild.put(guildId, false);
            DatabaseDAO database = new DatabaseDAO();
            database.saveOnlyVoice(guildId, false);
            return false;
        }
        return onlyVoicePerGuild.get(guildId);
    }

    public static boolean checkOrInitDisableBot(String guildId) {
        if (disableBotPerGuild.get(guildId) == null) {
            disableBotPerGuild.put(guildId, false);
            DatabaseDAO database = new DatabaseDAO();
            database.saveOnlyVoice(guildId, false);
            return false;
        }
        return disableBotPerGuild.get(guildId);
    }

    public static List<String> checkOrInitBlacklist(String guildId) {
        if (blackListTextChannel.get(guildId) == null) {
            List<String> blackLists = new ArrayList<>();
            blackListTextChannel.put(guildId, blackLists);
            return blackLists;
        }
        return blackListTextChannel.get(guildId);
    }
    
    public static boolean checkOrInitNoInterrupt(String guildId) {
        if (noInterruptSoundPerGuild.get(guildId) == null) {
            noInterruptSoundPerGuild.put(guildId, true);
            DatabaseDAO database = new DatabaseDAO();
            database.saveNoInterrupt(guildId, true);
            return true;
        }
        return noInterruptSoundPerGuild.get(guildId);
    }
    
    public static boolean checkForEmplySlot(String name) {
        String pattern = "\\W*((?i)empty slot(?-i))\\W*";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(name);
        return m.find();
    }
}
