package services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.tritonus.share.sampled.file.TAudioFileFormat;

import net.dv8tion.jda.api.entities.Message.Attachment;
import soundBoard.SoundboardUtils;
import soundBoard.YoutubeDownload;

public class SaveService {
    private static final String seperator = File.separator;
    private static final String baseDir = System.getProperty("user.dir") + seperator;

    public SaveService() {
    }

    public String saveYoutubeSound(String guildId, String name, String url) {
        if (!createDirIfNotExists(guildId))
            return null;

        String filePath = youtubeVideoDownload(guildId, name, url);
        if (filePath == null)
            return null;
        return filePath;
    }

    private boolean createDirIfNotExists(String guildId) {
        File dir = new File(baseDir + guildId);
        if (dir.exists())
            return true;
        try {
            Files.createDirectories(dir.toPath());
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private String youtubeVideoDownload(String guildId, String name, String url) {
        if (!downloadSong(guildId, name, url))
            return null;

        if (name.contains(" ")) {
            name = name.replaceAll(" ", "_");
        }
        String filePath = baseDir + guildId + seperator + name + ".mp3";

        File file = new File(filePath);
        int duration = getDuration(file);
        Long size = -1L;
        try {
            size = Files.size(file.toPath());
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        if (duration == -1 || size > 8000000L) {
            System.out.println(duration);
            System.out.println(size);
            file.delete();
            return null;
        }
        if (duration > 30000) {
            file.delete();
            return null;
        }
        return filePath;
    }

    public String saveMediaFile(Attachment attachment, String guildId, String name) {
        if (!createDirIfNotExists(guildId))
            return null;

        File tempFile = new File(baseDir + guildId + seperator + attachment.getFileName());
        CompletableFuture<File> futureFile = attachment.getProxy().downloadToFile(tempFile);
        while (!futureFile.isDone()) {
        }

        File file;
        File renamedFile = null;
        try {
            file = futureFile.get();

            if (SoundboardUtils.isMP3File(file.getAbsolutePath())) {
                int duration = getDuration(file);
                Long size = Files.size(file.toPath());
                if (duration == -1 || size > 8000000L) {
                    return null;
                }
                if (duration > 30000) {
                    file.delete();
                    return null;
                }

                renamedFile = new File(file.getParent(), name + ".mp3");
                Files.move(file.toPath(), renamedFile.toPath());
                return renamedFile.getAbsolutePath();
            }
            file.delete();
        } catch (InterruptedException | ExecutionException | IOException e) {
            return null;
        }
        return null;
    }

    // Just to convert, useless otherwise
    // public Map<String, String> getAllSoundsAndUrl(String guildId, boolean
    // isTrigger) {
    // String dir;
    // if (isTrigger) {
    // dir = baseDir + guildId + seperator + guildId + "-triggers" + ".txt";
    // } else {
    // dir = baseDir + guildId + seperator + guildId + ".txt";
    // }
    //
    // try (BufferedReader br = new BufferedReader(new FileReader(dir))) {
    // String line;
    // Map<String, String> map = new HashMap<String, String>();
    // while ((line = br.readLine()) != null) {
    // String[] splited = line.split("====");
    // if (isTrigger) {
    // map.put(splited[1], splited[0]);
    // } else {
    // map.put(splited[0], splited[1]);
    // }
    //
    // }
    // return map;
    // } catch (Exception e) {
    // return new HashMap<String, String>();
    // }
    // }

    public boolean removeFile(String guildId, String path) {
        File fileToRemove = new File(path);
        return fileToRemove.delete();
    }

    public String renameFile(String guildId, String oldPath, String newName) {
        Path fileToRename = new File(oldPath).toPath();
        try {
            return Files.move(fileToRename, fileToRename.resolveSibling(newName + ".mp3")).toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean downloadSong(String id, String name, String url) {
        YoutubeDownload download = new YoutubeDownload();
        if (name.contains(" ")) {
            name = name.replaceAll(" ", "_");
        }
        boolean hasDownloaded = download.executeCommand("yt-dlp -4 -x --audio-format mp3 --cookies cookies.txt -o "
                + baseDir + id + seperator + name + ".%(ext)s " + url);
        return hasDownloaded;
    }

    private static int getDuration(File file) {
        AudioFileFormat fileFormat;
        try {
            fileFormat = AudioSystem.getAudioFileFormat(file);

            if (fileFormat instanceof TAudioFileFormat) {
                Map<?, ?> properties = ((TAudioFileFormat) fileFormat).properties();
                String key = "duration";
                Long microseconds = (Long) properties.get(key);
                return (int) (microseconds / 1000);
            }
        } catch (UnsupportedAudioFileException | IOException e) {
            return -1;
        }
        return -1;
    }
}
