package audioManagement;

import java.awt.Color;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.entities.channel.middleman.AudioChannel;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.api.managers.AudioManager;
import services.MessageService;
import soundBoard.SoundboardUtils;
import soundBoard.resources.TextMessages;

public class AudioManagement {
    private static final Map<Long, GuildMusicManager> musicManagers = new HashMap<>();
    public static final Map<String, AudioPlayerManager> playerManagers = new HashMap<>();
    private static Map<Long, AudioManager> audioManager = new HashMap<Long, AudioManager>();

    public synchronized GuildMusicManager getGuildAudioPlayer(Guild guild, AudioPlayerManager playerManager) {
        long guildId = guild.getIdLong();
        GuildMusicManager musicManager = musicManagers.get(guildId);

        if (musicManager == null) {
            musicManager = new GuildMusicManager(playerManager);
            musicManagers.put(guildId, musicManager);
        }

        guild.getAudioManager().setSendingHandler(musicManager.getSendHandler());

        return musicManager;
    }

    public AudioPlayerManager getPlayerManager(String guildId) {
        AudioPlayerManager playerManager = playerManagers.get(guildId);
        if (playerManager == null) {
            playerManager = new DefaultAudioPlayerManager();
            AudioSourceManagers.registerRemoteSources(playerManager);
            AudioSourceManagers.registerLocalSource(playerManager);
            playerManagers.put(guildId, playerManager);
        }
        return playerManager;
    }

    public void loadAndPlay(final TextChannel channel, final String trackUrl, final Member member,
            AudioPlayerManager playerManager) {
        String guildId = channel.getGuild().getId();

        if (SoundboardUtils.disableBotPerGuild.get(guildId))
            return;

        if (SoundboardUtils.isYoutubeUrl(trackUrl)) {
            MessageService.sendMessage(channel, TextMessages.NOT_SUPPORTED.message, null, Color.RED);
            return;
        }

        if (!audioManager.containsKey(channel.getGuild().getIdLong()))
            audioManager.put(channel.getGuild().getIdLong(), channel.getGuild().getAudioManager());

        final GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild(), playerManager);

        playerManager.loadItemOrdered(musicManager, trackUrl, new AudioLoadResultHandler() {
            @Override
            public void trackLoaded(AudioTrack track) {
                play(channel.getGuild(), musicManager, track, member, channel);
            }

            @Override
            public void playlistLoaded(AudioPlaylist playlist) {
                AudioTrack firstTrack = playlist.getSelectedTrack();

                if (firstTrack == null) {
                    firstTrack = playlist.getTracks().get(0);
                }

                MessageService.sendMessage(channel, TextMessages.PLAYLIST_CALL.message, null, Color.RED);
            }

            @Override
            public void noMatches() {
                MessageService.sendMessage(channel, "Nothing found by " + trackUrl, null, Color.RED);
            }

            @Override
            public void loadFailed(FriendlyException exception) {
                exception.printStackTrace();
            }
        });
    }

    public boolean changeVolume(Guild guild, int volume) {
        return getGuildAudioPlayer(guild, getPlayerManager(guild.getId())).scheduler.setVolume(volume);
    }

    public void skipTrack(Guild guild) {
        getGuildAudioPlayer(guild, getPlayerManager(guild.getId())).scheduler.nextTrack();
    }

    public void stopAll(Guild guild) {
        getGuildAudioPlayer(guild, getPlayerManager(guild.getId())).scheduler.getQueue().clear();
        skipTrack(guild);
    }

    public static boolean connectToVoiceChannel(AudioManager audioManager, Member member, TextChannel channel) {
        AudioChannel connectedChannel = member.getVoiceState().getChannel();
        
        List<String> blackList = SoundboardUtils.blackListTextChannel.get(connectedChannel.getGuild().getId());
        if (blackList != null) {
        	if (blackList.contains(connectedChannel.getId())) {
        		return false;
        	}
        }

        if (!audioManager.isConnected()) {
            try {
                audioManager.openAudioConnection(connectedChannel);
                audioManager.setSelfDeafened(true);
                return true;
            } catch (Exception e) {
                if (e instanceof InsufficientPermissionException) {
                    MessageService.sendMessage(channel, TextMessages.VOICE_CHANNEL_PERM.message, null, Color.RED);
                }
                return false;
            }
        }
        return true;
    }

    public static void disconnectFromVoiceChannel() {
        audioManager.forEach((k, v) -> {
            if (musicManagers.get(k).player.getPlayingTrack() == null) {
                v.closeAudioConnection();
                return;
            }
        });
    }

    private void play(Guild guild, GuildMusicManager musicManager, AudioTrack track, Member member,
            TextChannel channel) {
        if (!connectToVoiceChannel(guild.getAudioManager(), member, channel))
            return;

        musicManager.scheduler.queue(track, SoundboardUtils.checkOrInitNoInterrupt(guild.getId()));
    }
}
